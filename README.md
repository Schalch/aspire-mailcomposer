# Mail Composer Component - Aspire IQ

This component is a Proof of Concept, intented to provide users the ability to send messages to their important contacts via email.

[Figma Prototype View](https://www.figma.com/proto/LJBeNSCU6G8NE2cVKowG6B/Engineer-candidate-projects?node-id=671%3A272&viewport=-35%2C554%2C0.18268907070159912&scaling=scale-down-width)

This component enables the user to input multiple email addresses to which they want to send an email message by typing the email address and hitting the Enter or Tab button and also allowing the user to delete a previously entered email address. If the user enters an invalid email address, it displays an error indicator to the user.

For the autocomplete dropdown, normally the email addresses would be populated via an API call, but for this example it's a mocked API request with the following [data](https://docs.google.com/spreadsheets/d/1BqV34zGQUMaEZkTEEFJZet8VTffFM_zXY2alxFY07xM/edit#gid=0).

## TODO'S and Intended Developing Sequence

- Create React App;
- Project basic configs and repo;
- Initial documentation;
- Map sub-components based on prototype;
- Style and structure for the interface;
- Create behaviors using TDD.

### Possible Enhancements

- Lazy loading integration with a real API which provides a smaller return based on what the user is typing after 2 characters;
- Make it more accessible preparing tags with aria for screen readers;
- Add prop-types lib vendor for improving prop checking;
- Add styled-components for in-component theming;
- Improve responsiveness for proper behaviors on specific devices;
- Improve project configuration for linting and pre-commit checking;
- CI/CD building with Travis CI or Jenkins for automated deploy;
- Automated tests during the build pipeline with SonarQube for coverage and blocking in case of failing tests;
- UX (maybe with A/B) Testing for getting users experiences;

### 4hs, time's up!

Things that could not be finished within 4hs:

- Validation for input e-mails and badges;
- Fetch of a static JSON file to filter the mails suggestions according to user input;
- Structure, style and behavior for suggestions (a component: an absolute div with inner scrolling and a list of the filtered e-mails received as props);
- Tests for the component behaviors;
- Some nice CSS transitions for the badges and hovers;


## Project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation

### `yarn install`

Installs the project. Alternatively, `npm install` can be used in case _yarn_ isn't installed.

### `rm -rf node_modules && yarn install --frozen-lockfile`

Makes a clean install using the locked versions of the dependencies. Alternatively, `npm ci` can be used in case _yarn_ isn't installed.

## Available Scripts (after project installation)

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
