import { render, screen } from '@testing-library/react';
import EmailField from './EmailField';

describe('EmailField: Smoke Tests', () => {
  test('should render component', () => {
    render(<EmailField />);
    const emailFieldEl = screen.getByTestId('emailInputField');
    expect(emailFieldEl).toBeInTheDocument();
  });
});