import React, { useState, useCallback } from 'react';
import Badge from '../Badge/Badge';
import './EmailField.scss';

const EmailField = ({ ...rest }) => {
  const [ emailInput, setEmailInput ] = useState('');
  const [ arrMails, setArrMails ] = useState([]);

  const onHandleInput = evt => {
    setEmailInput(evt.target.value);
  };

  const onHandleSend = useCallback(evt => {
    const { keyCode } = evt;
    /*
       Enter and Tab keyCodes,
       with this approach would be easy to include more
    */
    const sendCodes = [13, 9];
    const alreadyHasMail = mail => arrMails.indexOf(mail) >= 0;

    if (sendCodes.indexOf(keyCode) > -1) {
      evt.preventDefault();
      const { target: { value } } = evt;
      if (value && !alreadyHasMail(value)) {
        setArrMails([...arrMails, value]);
        setEmailInput('');
      }
    }
  }, [arrMails]);

  const onBadgeClick = useCallback((evt, mail)  => {
    /*
      Prevent default bubbling to avoid triggering
      label default behavior
    */
    evt.preventDefault();
    const mails = [ ...arrMails ];
    const mailIndex = mails.findIndex(item => item === mail);
    mails.splice(mailIndex, 1);
    setArrMails(mails);
  }, [arrMails]);

  return <div className="field-container" { ...rest }>
    <label htmlFor="emailField">
      { arrMails.map((email, index) => <Badge
                                key={index}
                                onBadgeClick={evt => onBadgeClick(evt, email)}
                                >
                                {email}
                              </Badge>) }
      <input data-testid="emailInputField"
            id="emailField"
            type="text"
            placeholder="theresa@email.com"
            onKeyDown={ onHandleSend }
            onChange={ onHandleInput }
            value={emailInput} />
    </label>
  </div>;
}

export default EmailField;