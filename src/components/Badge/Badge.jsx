import React from 'react';
import './Badge.scss';

const Badge = ({ onBadgeClick, children, ...rest }) => <span
  className="badge"
  onClick={onBadgeClick} { ...rest }>
    { children }
</span>

export default Badge;