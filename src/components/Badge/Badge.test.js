import { render, screen } from '@testing-library/react';
import Badge from './Badge';

describe('Badge: Smoke Tests', () => {
  test('should render component', () => {
    render(<Badge>test</Badge>);
    const BadgeTxt = screen.getByText(/test/g);
    expect(BadgeTxt).toBeInTheDocument();
  });
});