import EmailField from './components/EmailField/EmailField';
import './App.scss';

function App() {
  return (
    <div data-testid="appContainer" className="App">
      <EmailField />
    </div>
  );
}

export default App;
