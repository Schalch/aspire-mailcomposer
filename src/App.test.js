import { render, screen } from '@testing-library/react';
import App from './App';

test('should render app container', () => {
  render(<App />);
  const appContainerEl = screen.getByTestId('appContainer');
  expect(appContainerEl).toBeInTheDocument();
});
